===============
Final Activity
===============

-- 1.

SELECT customerName FROM customers WHERE country LIKE "Philippines";

-- 2.

SELECT contactLastName,contactFirstName FROM customers WHERE customerName LIKE "La Rochelle Gifts";

-- 3.

SELECT productName,MSRP FROM products WHERE productName LIKE "%Titanic";

-- 4.

SELECT lastName,firstName FROM employees WHERE email LIKE "jfirrelli@classicmodelcars.com";

-- 5.

SELECT customerName FROM customers WHERE state IS NULL;

-- 6.

SELECT firstName,lastName,email FROM employees WHERE lastName LIKE "Patterson" AND firstName LIKE "Steve";

-- 7.

SELECT customerName,country,creditLimit FROM customers WHERE country NOT LIKE "USA" AND creditLimit > 3000;

-- 8.

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9.

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10.

SELECT DISTINCT country FROM customers;

-- 11.

SELECT DISTINCT status FROM orders;

-- 12.

SELECT customerName,country FROM customers WHERE country LIKE "USA" OR country LIKE "France" OR country LIKE "Canada";

-- 13.

SELECT firstName,lastName,city FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE city LIKE "Tokyo";

-- 14.

SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.EmployeeNumber WHERE firstName LIKE "Leslie" and lastName LIKE "Thompson";

-- 15.

SELECT productName,customerName FROM customers JOIN orders ON customers.customerNumber = orders.customerNumber JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber JOIN products ON orderdetails.productCode = products.productCode WHERE customerName LIKE "Baane Mini Imports";

-- 16.

SELECT lastName,firstName,customerName,customers.country FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber JOIN offices ON employees.officeCode = offices.officeCode WHERE customers.country = offices.country;

-- 17.

SELECT productName,quantityInStock FROM products WHERE productLine LIKE "planes" AND quantityInStock < 1000;

-- 18.

SELECT customerName FROM customers WHERE phone LIKE "+81%";

============
STRETCH GOAL
============

-- 1.

SELECT productName FROM customers JOIN orders ON customers.customerNumber = orders.customerNumber JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber JOIN products ON orderdetails.productCode = products.productCode WHERE customerName LIKE "Baane Mini Imports";

-- 2.

SELECT e.lastName,e.firstName FROM employees e INNER JOIN employees m ON m.employeeNumber = e.reportsTo WHERE m.firstName LIKE "Anthony" AND m.lastName LIKE "Bow";

-- 3.

SELECT productName FROM products WHERE MSRP = (SELECT MAX(MSRP) FROM products);

-- 4.

SELECT COUNT(productName),productLine FROM products GROUP BY productLine;

-- 5.

SELECT COUNT(orderNumber) FROM orders WHERE status LIKE "cancelled";